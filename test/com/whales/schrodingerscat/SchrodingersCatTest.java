/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.whales.schrodingerscat;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author oyewale
 */
public class SchrodingersCatTest {
    
    private SchrodingersCat schrodingersCat;
    
    private static final String TEST_TRANSFROM = "Hello";
    private static final String TEST_TRANSFROM_OUTPUT = "Hello";
    private static final int TEST_TRANSFORM_COST = 0;
    
    private static final String TEST_TRANSFROM_UNEQUAL_OUTPUT = "Puppy";
    private static final int TEST_TRANSFORM_UNEQUAL_COST = 5;
    
    private static final String TEST_TRANSFROM_MISMATCH = "Helloo";
    
    public SchrodingersCatTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        schrodingersCat = new SchrodingersCat();
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of main method, of class SchrodingersCat.
     */
    @Test
    public void testMain() {
        System.out.println("Testing Main Method");
        String[] args = null;
        SchrodingersCat.main(args);
        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
    }
    
    /**
     * Test of transform method, of class SchrodingersCat
     */
    @Test
    public void testTransformEquals(){        
        System.out.println("Testing Transform Method");
        int transformResult = schrodingersCat.transform(TEST_TRANSFROM, TEST_TRANSFROM_OUTPUT);
        assertEquals(TEST_TRANSFORM_COST, transformResult );
    }
    
    /**
     * Test of transform method, of class SchrodingersCat
     */
    @Test
    public void testTransformNotEquals(){        
        System.out.println("Testing Transform Method, Unequal");
        int transformResult = schrodingersCat.transform(TEST_TRANSFROM, TEST_TRANSFROM_UNEQUAL_OUTPUT);
        assertEquals(TEST_TRANSFORM_UNEQUAL_COST, transformResult );
    }
    
    /**
     * Test of transform method, of class SchrodingersCat
     */
    @Test(expected = IllegalArgumentException.class)
    public void testTransformStringMismatch(){        
        System.out.println("Testing Transform String Mismatch");
        int transformResult = schrodingersCat.transform(TEST_TRANSFROM, TEST_TRANSFROM_MISMATCH);        
    }
    
}
