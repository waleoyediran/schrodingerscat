/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.whales.schrodingerscat;

/**
 *
 * @author oyewale
 */
public class SchrodingersCat {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
    }
    
    /**
     * 
     * @param input
     * @param input2 
     * @return transformedString
     */
    public int transform (String input, String input2){
        String transformedString = input;
        int transformCost = 0;
        
        // Validate that the strings have equal length
        if (input.length() != input2.length())
            throw new IllegalArgumentException(
                    "The String arguments have unequal length");
        
        for (int i = 0; i < input.length(); i++){
            if (input.charAt(i) != input2.charAt(i))
                transformCost++;
        }
        
        return transformCost;
    }
    
}
